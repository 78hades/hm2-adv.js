const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];
  const list = document.createElement('ul')
  const cell = document.getElementById('root')
  function createList(array) {
    for (let i = 0; i < array.length; i++) {
        const line = document.createElement('li');
        let options = ['name', 'author', 'price'];
    try {
            for (let key of options) {
                if (!array[i].hasOwnProperty(key)) {
                   throw new Error(`Елемент ${i + 1} не має властивості ${key}`);
                }
                line.innerHTML += `${array[i][key]}, `;
               
            }
            list.appendChild(line);
            
        }
     catch (err) {
        console.log(err.message);
    }
    }}

cell.appendChild(list);
createList(books);